const express = require("express");
const userController = require("../controllers/userController.js");
const userRouter = express.Router();
 
userRouter.post("/create", userController.postUser);
userRouter.put("/:id", userController.putUser);
userRouter.get("/:id", userController.getUser);
userRouter.delete("/:id", userController.deleteUser);
 
module.exports = userRouter;