const express = require("express");
const adminController = require("../controllers/adminController.js");
const adminRouter = express.Router();

adminRouter.post("/create", adminController.postAdmin);
adminRouter.put("/:id", adminController.putAdmin);
adminRouter.get("/:id", adminController.getAdmin);
adminRouter.delete("/:id", adminController.deleteAdmin);

module.exports = adminRouter;