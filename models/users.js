const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength:3,
        maxlength:20,
        default:"noname"
    },
    id: {
        type: Number,
        required: true,
        min: 1,
        max:100,
        default:0
    },
    age: {
        type:Number,
        required:true,
        min:1,
        max:120,
        default:1
    }

},
{versionKey: false }
);
const User = mongoose.model("User", userScheme);
module.exports = User;