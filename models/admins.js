const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 3,
        maxlength: 20
    },
    id: {
        type: Number,
        required: true,
        min: 1,
        max: 100
    },
    key: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 10,
        default: "noKey"
    }

},
    { versionKey: false }
);
const Admin = mongoose.model("Admin", adminScheme);
module.exports = Admin;