const Admin = require("../models/admins.js");

module.exports = {
    postAdmin : (request, response) => {

    Admin.create({ name: request.body.name, id: request.body.id, key: request.body.key }, function (err, doc) {

        if (err) return console.log(err);

        response.send("<h1>Данные внесены</h1><p>id = " + doc + "</p>");
    });
},

    putAdmin : (request, response) => {
    mongoose.set("useFindAndModify", false);
    Admin.findOneAndUpdate({ id: request.params.id }, { id: request.body.id, name: request.body.name, key: request.body.key }, { new: true }, function (err, user) {
        if (err) return console.log(err);

        response.send("<h1>Обновление пользователя с</h1><p>id = " + admin);
    });
},

    getAdmin : (request, response) => {
    Admin.find({ id: request.params.id }, function (err, docs) {

        if (err) return console.log(err);

        response.send("<h1>Получение данных пользователя</h1><p>id=" + docs + "</p>");
    });

},

    deleteAdmin : (request, response) => {
    Admin.findOneAndDelete({ id: request.params.id }, function (err, doc) {

        if (err) return console.log(err);

        response.send("<h1>Удаление пользователя</h1><p>id=" + doc + "</p>")
    });

}
}